#pragma once
#include <SDL.h>
class Hinh
{
public:
	Hinh() {};
	~Hinh() {};
	virtual void Ve(SDL_Renderer *ren) = 0;
	virtual void TinhTien(int, int) = 0;
};

