#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc-x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc-y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	
    // Area 2

}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x, y, p;
	x = 0;
	y = b;
	p = (b*b) - (a*a*b) + ((a*a) / 4);
	Draw4Points(xc, yc, x, y, ren);
	while ((2 * x*b*b)<(2 * y*a*a))
	{

		if (p < 0)
		{
			p += (2 * b*b*x) + (b*b);
		}
		else
		{
			p += (2 * b*b*x + b * b) - (2 * a*a*y);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	p = ((float)x + 0.5)*((float)x + 0.5)*b*b + (y - 1)*(y - 1)*a*a - a * a*b*b;
	Draw4Points(xc, yc, x, y, ren);
	while (y >= 0)
	{

		if (p > 0)
		{
			p =p-(2 * a*a*y) + (a*a);
		}
		else
		{
			x++;
			p += (2 * b*b*x) - (2 * a*a*y) - (a*a);
		}
		y--;
		Draw4Points(xc, yc, x, y, ren);
	}
	
}