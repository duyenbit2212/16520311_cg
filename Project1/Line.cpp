﻿#include "Line.h"
//DDA algorithm
void DDA_Line1(int dx, int dy, float m, float x, float y, int x2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (x < x2)
	{
		x = x + 1;
		y = y + m;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}
}

void DDA_Line2(int dx, int dy, float m, float x, float y, int x2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (x > x2)
	{
		x = x - 1;
		y = y - m;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}
}
void DDA_Line3(int dx, int dy, float dm, float x, float y, int y2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (y < y2)
	{
		y = y + 1;
		x = x + dm;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}
}

void DDA_Line4(int dx, int dy, float dm, float x, float y, int y2, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	while (y > y2)
	{
		y = y - 1;
		x = x - dm;
		SDL_RenderDrawPoint(ren, x, int(y + 0.5));
	}
}
void DDA_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int dx = x2 - x1;
	int dy = y2 - y1;
	float m = (float)dy / dx;
	float dm = (float)dx / dy;
	float x = x1;
	float y = y1;

	if (dx == 0 || dy == 0)
		LineAxis(x1, y1, x2, y2, dx, dy, ren);
	else
	{
		//1: 0 < m < 1 & dx > 0; -1 < m < 0 & dx > 0
		if ((m > 0 && m <= 1 && dx > 0) || (m >= -1 && m < 0 && dx > 0))
		{
			DDA_Line1(dx, dy, m, x, y, x2, ren);
		}
		//2: 0 < m < 1 & dx < 0; -1 < m < 0 & dx < 0
		else if ((m > 0 && m <= 1 && dx < 0) || (m >= -1 && m < 0 && dx < 0))
		{
			DDA_Line2(dx, dy, m, x, y, x2, ren);
		}
		//3: m > 1 & dy > 0; m < -1 & dy > 0
		else if ((m >= 1 && dy > 0) || (m <= -1 && dy > 0))
		{
			DDA_Line3(dx, dy, dm, x, y, y2, ren);
		}
		//4: m > 1 & dy < 0; m < -1 & dy < 0
		else if ((m >= 1 && dy < 0) || (m <= -1 && dy < 0))
		{
			DDA_Line4(dx, dy, dm, x, y, y2, ren);
		}
	}
}

// Bresenham algorithm
void Bresenham1(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void Bresenham2(int x1, int y1, int x2, int y2, SDL_Renderer *ren);
void Bresenham_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	float m = 0;
	if (x1 == x2)
	{
		for (int y = y1; y <= y2; y++)
			SDL_RenderDrawPoint(ren, x1, y);
	}
	else if (y1 == y2)
	{
		for (int x = x1; x <= x2; x++)
			SDL_RenderDrawPoint(ren, x, y1);
	}
	else
	{
	 m = float(y2 - y1) / (x2 - x1);
	}
	if (0 < m && m < 1)
		Bresenham1(x1, y1, x2, y2, ren);
	else if (m>=1)
		Bresenham2(x1, y1, x2, y2, ren);
	/* else if (0>m && m>-1)
	Bresenham3(x1,y1,x2,y2);
	else	Bresenham4(x1,y1,x2,y2)
	T?m th?i ch?a xét ??n
	*/
}
void Bresenham1(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int x, y, p;
	int Dx = x2 - x1;
	int Dy = y2 - y1;
	p = 2 * Dy - Dx;
	x = x1;
	y = y1;
	while (x < x2)
	{
		if (p > 0)
		{
			y++;
			p += 2 * (Dy - Dx);
		}
		else
		{
			p += 2 * Dy;
		}
		x++;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
void Bresenham2(int x1, int y1, int x2, int y2, SDL_Renderer *ren)
{
	int x, y, p;
	int Dx = x2 - x1;
	int Dy = y2 - y1;
	p = 2 * Dx - Dy;
	x = x1;
	y = y1;
	while (y < y2) {
		if (p > 0)
		{
			x++;
			p += 2 * (Dx - Dy);
		}
		else
		{
			p += 2 * Dx;
		}
		y++;
		SDL_RenderDrawPoint(ren, x, y);
	}
}
void Midpoint_Line(int x1, int y1, int x2, int y2, SDL_Renderer *ren) {
	int x, y;
	float p;
	int Dx = x2 - x1;
	int Dy = y2 - y1;
	x = x1;
	y = y1;
	p = Dy - (Dx >> 1); // ~ Dy -Dx/2
	while (x <= x2)
	{
		x++;
		if (p < 0)
		{
			p += Dy;
		}
		else
		{
			p += Dy - Dx;
			y++;
		}
		SDL_RenderDrawPoint(ren, x, y);
	}
}
void LineAxis(int x1, int y1, int x2, int y2, int dx, int dy, SDL_Renderer *ren)
{
	//Line has dx = 0 OR dy = 0
	// Ý là x2==x1 OR y2==y1. Trường hợp này sv đã viết chung trong hàm Bresenham
	if (dy < 0)
	{
		for (int y = y1; y >= y2; y--)
			SDL_RenderDrawPoint(ren, x1, y);
	}
	if (dx < 0)
	{
		for(int x=x1;x>=x2;x--)
			SDL_RenderDrawPoint(ren, x, y1);
	}
	if (dx==0)
	{
		for (int y = y1; y <= y2; y++)
			SDL_RenderDrawPoint(ren, x1, y);
	}
	else if (dy==0)
	{
		for (int x = x1; x <= x2; x++)
			SDL_RenderDrawPoint(ren, x, y1);
	}

}


