#pragma once
#include <SDL.h>
void Put8Pixel(int x0, int y0, int x, int y, SDL_Renderer *ren);
void CircleMidPoint(int x0, int y0, int R, SDL_Renderer *ren);
void CircleBresenham(int x0, int y0, int r, SDL_Renderer *ren);