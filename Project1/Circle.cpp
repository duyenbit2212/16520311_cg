#include "Circle.h"
void Put8Pixel(int x0, int y0,int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, x0+x, y0+y);
	SDL_RenderDrawPoint(ren, x0-x, y0+y);
	SDL_RenderDrawPoint(ren, x0+x, y0-y);
	SDL_RenderDrawPoint(ren, x0-x, y0-y);
	SDL_RenderDrawPoint(ren, x0+y, y0+x);
	SDL_RenderDrawPoint(ren, x0-y, y0+x);
	SDL_RenderDrawPoint(ren, x0+y, y0-x);
	SDL_RenderDrawPoint(ren, x0-y, y0-x);
}
void CircleMidPoint(int x0, int y0,int R, SDL_Renderer *ren)
{
int x, y,p;
x = 0;
y = R;
Put8Pixel(x0,y0,x,y,ren);
p = 1 - R;
while (x < y)
{
	if (p < 0)
		p += 2 * x + 3;
	else
	{
		p += 2 * (x - y) + 5;
		y--;
	}
	x++;
	Put8Pixel(x0,y0,x, y, ren);
}
}
void CircleBresenham(int x0, int y0, int r, SDL_Renderer *ren)
{
	int x = 0, y = r;
	int p = 3 - 2 * r;
	while (x <= y)
	{
		Put8Pixel(x0, y0, x, y, ren);
		if (p < 0)
			p += 4 * x + 6;
		else
		{
			p += 4 * (x - y) - 10;
			y = y - 1;
		}
		x = x + 1;
	}
}